<html>
	<head>
		<title>Laravel Learning | @yield('title', 'Home Page') </title>
		<link rel="stylesheet" href="{{asset('css/style.css') }}" />
		<link rel="stylesheet" href="{{asset('css/genericons.css') }}" />
		<link rel="stylesheet" href="{{asset('css/reset.css') }}" />
		@section('links')
		@show
		@section('scripts')
		@show
		<style>
			.content {
				height:auto;
				margin-top:70px;
				margin-left:10px;
				float:left;				
			}
		</style>
	</head>
	<body>
		{{--	HEADER	--}}
		@include('layouts.header')		
		
		{{--	SIDEBAR	--}}
		@include('layouts.sidebar', ['sub_items' => $sub_items, 'menu_items' => $menu_items])
		
		{{--	SPECIFIC MODULE CONTENT	--}}
		<div class='content'>
		@section('content')		
		@show
		</div>
		
		{{--	FOOTER	--}}
		@include('layouts.footer')
	</body>
</html>
